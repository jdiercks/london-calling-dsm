

<?php
/*
 * Implements hook_preprocess_view()
 */
function happy_preprocess_views_view_fields(&$vars) {
  if ($vars['view']->name == 'happy_titles') {
    // Include the CTools tools that we need.
    ctools_include('ajax');
    ctools_include('modal');
    // The view has two fields, title (not linked and no styles added), and NID (again,
    // no style added. They are available here as $vars['fields']->title and 
    // $vars['fields']->nid.
    $name = $vars['fields']['title']->content;
    // Create a path for the url that is like our hook_menu() declaration above.
    $href = 'happy/nojs/' . $vars['fields']['nid']->content;
    // Here's the ctools function that generates the trigger inside the link
    // ctools_modal_text_button($text, $dest, $alt, $class = '')
    // http://api.drupalize.me/api/drupal/function/ctools_modal_text_button/7
    // IMPORTANT: Include ctools-modal-[your declared style name] as a class so 
    // Ctools knows what Javascript settings to use in generating the modal:
    $vars['ctools_link'] = ctools_modal_text_button($name, $href, t('View node content for @name', array('@name' => $name)), 'ctools-modal-happy-modal-style');
  }

  print $ctools_link;
}
?>