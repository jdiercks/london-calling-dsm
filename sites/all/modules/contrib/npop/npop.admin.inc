<?php

/**
 * @file
 * Node popup module administration pages functions.
 */

/**
 * Main administration settings page form.
 */
function npop_admin_settings_form($form, $form_state) {
  $form['npop_change_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change url'),
    '#default_value' => variable_get('npop_change_url', 1),
    '#description' => t('Allow to change url (by JavaScript History API), when popup window is oppen. This is SEO friendly settings for popup window'),
  );
  return system_settings_form($form);
}
