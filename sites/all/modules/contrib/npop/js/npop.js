(function ($) {
  'use strict';

  /**
   * Create url change by History API ajax command.
   *
   * @param {object} ajax Global Drupal.ajax object.
   * @param {object} response Response data.
   * @param {string} status Status of command.
   */
  Drupal.ajax.prototype.commands.npop_change_url = function (ajax, response, status) {
    var isHistoty = !!(window.history && history.pushState);
    if (isHistoty) {
      if (response.hasOwnProperty('toparent')) {
        history.pushState(null, null, $(response.toparent).data('parnet'));
      }
      else {
        history.pushState(null, null, response.url);
      }
    }
  };
}(jQuery));
